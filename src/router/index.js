import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/Main'
import Index from '@/components/Pages/Index'
import SingleImage from '@/components/Pages/SingleImage'
import Registration from '@/components/Pages/Registration'
import Login from '@/components/Pages/Login'
import Upload from '@/components/Pages/Upload'
import MyDownloads from '@/components/Pages/MyDownloads'
import MyUploads from '@/components/Pages/MyUploads'
import ImageProgress from '@/components/Pages/ImageProgress'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main,
      children: [
        {
          path: '',
          name: 'Index',
          component: Index
        },
        {
          path: 'image/:image_id',
          name: 'SingleImage',
          component: SingleImage
        },
        {
          path: 'image-progress/:image_id',
          name: 'ImageProgress',
          component: ImageProgress
        },
        {
          path: 'register',
          name: 'Registration',
          component: Registration
        },
        {
          path: 'login',
          name: 'Login',
          component: Login
        },
        {
          path: 'my-uploads',
          name: 'MyUploads',
          component: MyUploads
        },
        {
          path: 'my-downloads',
          name: 'MyDownloads',
          component: MyDownloads
        },
        {
          path: 'upload',
          name: 'Upload',
          component: Upload
        }
      ]
    }
  ],
  mode: 'history'
})
